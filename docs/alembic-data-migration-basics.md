# Alembic — 데이터 마이그레이션 기본사항
Alembic은 Python 어플리케이션을 위한 인기 있는 데이터베이스 마이그레이션 도구이다. 데이터베이스 스키마 변경과 버전을 관리하는 데 탁월한 여러 가지 장점과 이점을 제공한다. 다음은 **Alembic 사용 시 얻을 수 있는 몇 가지 주요 이점들**이다.

1. **데이터베이스 스키마 버전 관리**: Alembic을 사용하면 데이터베이스 스키마의 버전을 제어할 수 있다. 각 데이터베이스 마이그레이션은 별도의 버전으로 기록되므로 변경 사항을 쉽게 추적하고 필요한 경우 이전 버전으로 롤백할 수 있다.
1. **데이터베이스 마이그레이션 관리**: Alembic은 데이터베이스 마이그레이션 관리 프로세스를 간소화한다. SQL 모델 변경에 따라 마이그레이션 스크립트를 자동으로 생성할 수 있어, SQL 마이그레이션 스크립트를 수동으로 작성하는 수고를 덜어준다.
1. **자동 마이그레이션**: Alembic은 멱등(idempotent) 마이그레이션을 생성하므로 문제나 데이터 중복 없이 동일한 마이그레이션을 여러 번 적용할 수 있다.
1. **여러 데이터베이스 엔진 지원**: Alembic은 PostgreSQL, MySQL, SQLite, Oracle 등을 포함한 다양한 데이터베이스 엔진과 호환된다. 이러한 유연성 덕분에 다양한 데이터베이스로 원활하게 작업할 수 있다.
1. **간소화된 스키마 진화**: Alembic을 사용하면 데이터 손실이나 복잡한 수동 작업 없이도 시간이 지남에 따라 데이터베이스 스키마를 쉽게 발전시킬 수 있다.
1. **롤백 지원**: Alembic은 마이그레이션을 롤백할 수 있는 간단한 방법을 제공하므로 필요한 경우 데이터베이스의 이전 상태로 되돌릴 수 있다.
1. **테스트와 지속적 통합**: 테스트 프레임워크와 지속적 통합 도구는 Alembic과 잘 통합된다. 배포 또는 테스트 프로세스 중에 마이그레이션을 자동으로 적용할 수 있다.
1. **협업과 팀워크**: 버전 제어 마이그레이션을 통해 여러 개발자가 동일한 프로젝트에서 더 쉽게 작업하고 데이터베이스 변경 사항을 효율적으로 적용할 수 있다.
1. **SQLAlchemy와 통합**: 어플리케이션에서 ORM(객체 관계형 매핑) 라이브러리로 SQLAlchemy를 사용하는 경우, Alembic은 이 라이브러리와 원활하게 통합되어 기존 SQLAlchemy 모델을 활용하여 마이그레이션을 생성한다.
1. **재현성과 일관성**: Alembic에서 생성된 데이터베이스 마이그레이션은 환경 전반에서 재현 가능하고 일관성이 있어 개발, 스테이징 및 프로덕션 데이터베이스가 동기화되도록 보장한다.
1. **자동화된 마이그레이션 스크립트 생성**: Alembic은 SQLAlchemy 모델의 변경 사항을 기반으로 마이그레이션 스크립트를 자동으로 생성하여 마이그레이션 스크립트를 수동으로 작성할 때 발생할 수 있는 인적 오류의 가능성을 줄여준다.

![](./1_hcdkMB2AGRgmodydlCppPw.webp)

## 먼저 5단계로 버전을 설정하고 확인한다.

### 1단계: Alembic 구성 파일 만들기

- 프로젝트 디렉토리에 알레믹 구성 설정을 저장할 `alembic.ini`라는 파일을 만든다. 이 파일은 데이터베이스 연결 URI와 기타 구성을 정의한다.
- 예 alembic.ini 파일 내용

```
# alembic.ini
[alembic]
script_location = alembic
sqlalchemy.url = driver://user:password@localhost/dbname
```

- `driver`, `user`, `password`, `localhost` 및 `dbname`을 실제 데이터베이스 연결 세부 정보로 바꾼다.

### 2단계: 얼렘빅 환경 디렉터리 생성

- 프로젝트 디렉터리에 Alembic 마이그레이션 스크립트와 버전을 저장할 디렉터리를 `alembic`이라는 이름으로 만든다.
- 터미널 또는 명령 프롬프트에서 다음 명령을 실행한다.

```bash
$ alembic init alembic
```

### 3단계: Alembic 구성 변경

- 새로 생성한 `alembic/env.py` 파일을 열고 `run_migrations_online()` 함수를 찾는다.
- 이 함수를 수정하여 `alembic.ini` 파일의 구성을 사용할 수 있도록 한다.

```python
from alembic import context
from sqlalchemy import engine_from_config, pool
from logging.config import fileConfig

# ...

def run_migrations_online():
    # ...

    connectable = engine_from_config(
        config.get_section(config.config_ini_section),
        prefix='sqlalchemy.',
        poolclass=pool.NullPool,
    )

    # ...
```

### 4단계: 초기 마이그레이션 생성

- 초기 마이그레이션을 생성하려면 터미널 또는 명령 프롬프트에서 다음 명령을 실행한다.

```bash
$ alembic revision — autogenerate -m “initial”
```

이 명령은 데이터베이스 스키마의 현재 상태를 기반으로 초기 마이그레이션 스크립트를 생성한다.

### 5단계: 초기 마이그레이션 적용

- 초기 마이그레이션을 적용하고 데이터베이스 테이블을 생성하려면 다음 명령을 실행한다.

```bash
$ alembic upgrade head
```

- 이 명령은 마이그레이션 스크립트를 실행하고 데이터베이스 스키마를 최신 버전으로 업데이트한다.
- Alembic 버전을 확인하려면 다음 명령을 실행한다.

```bash
$ alembic — version
```

- 그러면 Python 환경에 설치된 Alembic의 현재 버전이 표시된다.
- 이 단계를 통해 프로젝트에서 Alembic을 설정하고 초기 마이그레이션을 수행했다. 이제 Alembic을 사용하여 데이터베이스 스키마 변경 사항을 효과적으로 관리할 준비가 되었다.

## Alembic에 버전 관리를 통합

### 1. 버전 관리 통합이란?
버전 제어를 통해 코드와 데이터베이스 스키마의 변경 사항을 추적하고 관리할 수 있다. Alembic은 버전 관리 시스템과 원활하게 통합되어 협업을 간소화한다.

### 2. 버전 관리 통합의 이점

- 데이터베이스 스키마 변경 내역을 추적할 수 있다.
- 손쉬운 협업과 팀워크
- 이전 스키마 버전으로 롤백
- 동시 변경 시 충돌 방지

### 3. Alembic과 Git 통합

- Alembic은 널리 사용되는 분산 버전 관리 시스템인 Git을 지원한다.
- 데이터베이스 마이그레이션 스크립트는 Git 리포지토리의 일부가 된다.
- 팀원들과 협업하고, 변경 사항을 검토하고, 충돌을 해결하세요.

### 4. 버전 마이그레이션 스크립트
 
- 각 마이그레이션 스크립트는 데이터베이스 스키마 버전을 나타낸다.
- 마이그레이션 스크립트에는 설명이 포함된 이름을 사용한다 (예: "add_users_table").
- 일관성을 유지하기 위해 커밋된 마이그레이션 스크립트를 변경하지 않는다.

### 5. 버전 관리 통합의 모범 사례

- 마이그레이션 스크립트를 어플리케이션 코드 변경과 별도로 커밋한다.
- 명확성과 문서화를 위해 의미 있는 커밋 메시지를 사용한다.
- 복잡한 스키마 변경을 위한 기능 브랜치를 만든다.

### 6. 협업 워크플로우

- 마이그레이션을 실행하기 전에 리포지토리에서 최신 변경 사항을 가져온다.
- 충돌을 피하기 위해 팀원들과 조율한다.
- 풀 리퀘스트를 통해 마이그레이션을 검토하고 승인한다.

버전 관리 통합으로 원활하고 체계적인 개발 프로세스를 보장하여 팀과 협업하면서 데이터베이스 스키마 변경 사항을 더 쉽게 관리하고 추적할 수 있다.

## Alembic 명령어

![](./0_Wx579nkMPVHOrApy.webp)


| Command           | Description                                                                           |
|-------------------|---------------------------------------------------------------------------------------|
| branches          | Show current branch points.                                                           |
| check             | Check if revision command with autogenerate has pending upgrade ops.                  |
| current           | Display the current revision for a database.                                          |
| downgrade         | Revert to a previous version.                                                         |
| edit              | Edit revision script(s) using $EDITOR.                                                |
| ensure_version    | Create the alembic version table if it doesn't exist already.                         |
| heads             | Show current available heads in the script directory.                                 |
| history           | List changeset scripts in chronological order.                                        |
| init              | Initialize a new scripts directory.                                                   |
| list_templates    | List available templates.                                                             |
| merge             | Merge two revisions together. Creates a new migration file.                           |
| revision          | Create a new revision file.                                                           |
| show              | Show the revision(s) denoted by the given symbol.                                     |
| stamp             | 'stamp' the revision table with the given revision; don't run any migrations.         |
| upgrade           | Upgrade to a later version.                                                           |

전반적으로 Alembic은 데이터베이스 스키마 변경 관리 프로세스를 간소화하고, 팀 협업을 강화하며, 데이터베이스 마이그레이션의 일관성과 안정성을 보장한다. 시간이 지남에 따라 애플리케이션의 데이터베이스 스키마가 진화하는 과정에서 발생하는 복잡성을 처리하는 데 드는 시간과 노력을 절약할 수 있는 강력한 도구이다.
